package simpleOperationsAndCalculationsExercise;

import java.util.Scanner;

public class CharityCampaign {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int days = input.nextInt();
		int confectioner = input.nextInt();
		int cakes = input.nextInt();
		int waffles = input.nextInt();
		int pancakes = input.nextInt();
		
		double cakesSum = 45 * cakes;
		double wafflesSum = 5.80 * waffles;
		double pancakesSum = 3.20 * pancakes;
		
		double sumDay = (cakesSum + wafflesSum + pancakesSum) * confectioner;
		double campaignSum = sumDay * days;
		double totalSum = campaignSum - (campaignSum / 8);
		
		System.out.printf("%.2f", totalSum);
		
		input.close();
	}

}
