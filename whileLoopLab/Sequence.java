package whileLoopLab;

import java.util.Scanner;

public class Sequence {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int n = input.nextInt();
		int nextNumber = 1;
		
		while(nextNumber <= n) {
			System.out.println(nextNumber);
			nextNumber = nextNumber * 2 + 1;
			
		}
		
		input.close();
	}

}
