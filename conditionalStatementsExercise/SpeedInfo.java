package conditionalStatementsExercise;

import java.util.Scanner;

public class SpeedInfo {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double speed = input.nextDouble();
		String message = "";
		
		if(speed <= 10) {
			message = "slow";
		} else if(speed > 10 && speed <= 50) {
			message = "average";
		} else if(speed > 50 && speed <= 150) {
			message = "fast";
		} else if(speed > 150 && speed <= 1000) {
			message = "ultra fast";
		} else {
			message = "extremely fast";
		}
		
		System.out.println(message);
		
		input.close();
	}

}
