package nestedConditionalStatementsExercise;

import java.util.Scanner;

public class SummerOutfit {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int degree = Integer.parseInt(input.nextLine());
		String timeOfDay = input.nextLine();
		String message = "";
		String outfit = "";
		String shoes = "";
		
		if(degree >= 10 && degree <= 18) {
			switch(timeOfDay) {
				case "Morning":
					outfit = "Sweatshirt";
					shoes = "Sneakers";
				break;
				
				case "Afternoon":
					outfit = "Shirt";
					shoes = "Moccasins";
				break;
				
				case "Evening":
					outfit = "Shirt";
					shoes = "Moccasins";
				break;
			}
		} else if(degree > 18 && degree <= 24) {
			switch(timeOfDay) {
				case "Morning":
					outfit = "Shirt";
					shoes = "Moccasins";
				break;
				
				case "Afternoon":
					outfit = "T-Shirt";
					shoes = "Sandals";
				break;
				
				case "Evening":
					outfit = "Shirt";
					shoes = "Moccasins";
				break;
			}
		} else if(degree >= 25) {
			switch(timeOfDay) {
				case "Morning":
					outfit = "T-Shirt";
					shoes = "Sandals";
				break;
				
				case "Afternoon":
					outfit = "Swim Suit";
					shoes = "Barefoot";
				break;
				
				case "Evening":
					outfit = "Shirt";
					shoes = "Moccasins";
				break;
			}
		}
		
		message = "It's " + degree + " degrees, get your " + outfit +" and " + shoes + ".";
		System.out.println(message);
		
		input.close();
	}

}
