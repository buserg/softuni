package simpleOperationsAndCalculations;

import java.util.Scanner;

public class CelsiusToFarenheit {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double celsius = input.nextDouble();
		double farenheit = celsius * 1.8 + 32;
		
		System.out.printf("%.2f", farenheit);
		
		input.close();
	}

}
