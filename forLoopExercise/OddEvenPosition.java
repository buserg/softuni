package forLoopExercise;

import java.util.Scanner;

public class OddEvenPosition {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int n = Integer.parseInt(input.nextLine());
		
		double oddSum = 0.00;
		double oddMin = Double.MAX_VALUE;
		double oddMax = Double.NEGATIVE_INFINITY;
		double evenSum = 0.00; 
		double evenMin = Double.MAX_VALUE;
		double evenMax = Double.NEGATIVE_INFINITY;
		double number = 0.00;
		
		for (int i = 1; i <= n; i++) {
			number = Double.parseDouble(input.nextLine());
			
			if(i % 2 == 0) {
				evenSum += number;
				
				if(number < evenMin) {
					evenMin = number;
				}
				
				if(number > evenMax) {
					evenMax = number;
				}				
			} else {
				oddSum += number;
				
				if(number < oddMin) {
					oddMin = number;
				}
				
				if(number > oddMax) {
					oddMax = number;
				}
			}
		}
		
		if(n == 0) {
			System.out.println("OddSum=0.00,\nOddMin=No,\nOddMax=No,\nEvenSum=0.00,\nEvenMin=No,\nEvenMax=No");
		} else if(n == 1){
			System.out.printf("OddSum=%.2f,\nOddMin=%.2f,\nOddMax=%.2f,\nEvenSum=0.00,\nEvenMin=No,\nEvenMax=No", oddSum, oddMin, oddMax);
		} else {
			System.out.printf("OddSum=%.2f,\nOddMin=%.2f,\nOddMax=%.2f,\nEvenSum=%.2f,\nEvenMin=%.2f,\nEvenMax=%.2f", oddSum, oddMin, oddMax, evenSum, evenMin, evenMax);
			
		}
		
		
		input.close();
	}

}