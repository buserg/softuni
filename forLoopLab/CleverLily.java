package forLoopLab;

import java.util.Scanner;

public class CleverLily {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int age = Integer.parseInt(input.nextLine());
		double price = Double.parseDouble(input.nextLine());
		double toyPrice = Double.parseDouble(input.nextLine());
		double totalSum = 0;
		
		for (int i = 1; i <= age; i++) {
			if(i % 2 == 0) {
				totalSum += ((10 * (i / 2)) - 1);
			} else {
				totalSum += toyPrice;
			}
		}


		if(price > totalSum) {
			System.out.printf("No! %.2f", (price - totalSum));
		} else {
			System.out.printf("Yes! %.2f", (totalSum - price));
		}
		
		input.close();
	}
}