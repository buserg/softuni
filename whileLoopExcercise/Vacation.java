package whileLoopExcercise;

import java.util.Scanner;

public class Vacation {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double vocationMoney = Double.parseDouble(input.nextLine());
		double currentMoney = Double.parseDouble(input.nextLine());
		String action = "";
		double sum = 0;
		int countSpend = 0;
		int days = 0;
		
		while(true) {
			action = input.nextLine();
			sum = Double.parseDouble(input.nextLine());
			days++;
			
			if(action.equals("spend")) {
				currentMoney -= sum;
				if(currentMoney < 0) {
					currentMoney = 0;
				}
				
				countSpend++;
				
				if(countSpend == 5) {
					System.out.println("You can't save the money.\n" + days);
					
					break;
				}
			} else if(action.equals("save")) {
				currentMoney += sum;
				countSpend = 0;
			}
			
			if(currentMoney >= vocationMoney) {
				System.out.println("You saved the money for " + days + " days.");
				
				break;
			}
		}
		
		input.close();
	}
}
