package nestedConditionalStatementsExercise;

import java.util.Scanner;

public class NewHouse {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String flowersType = input.nextLine();
		int flowersNumber = input.nextInt();
		double budget = input.nextDouble();
		
		double rosePrice = 5.00;
		double dahliasPrice = 3.80;
		double tulipPrice = 2.80;
		double narcissusPrice = 3.00;
		double gladiolusPrice = 2.50;
		double totalSum = 0;
		double remainder = 0;
		String message = "";
		
		
		switch (flowersType) {
			case "Roses":
				totalSum = (flowersNumber * rosePrice);
				if(flowersNumber > 80) {
					totalSum = totalSum - (totalSum * 0.10);
				}
			break;
			
			case "Dahlias":
				totalSum = (flowersNumber * dahliasPrice);
				
				if(flowersNumber > 90) {
					totalSum = totalSum - (totalSum * 0.15);
				}
			break;
			
			case "Tulips":
				totalSum = (flowersNumber * tulipPrice);
				
				if(flowersNumber > 80) {
					totalSum = totalSum - (totalSum * 0.15);
				}
				
			break;
			
			case "Narcissus":
				totalSum = (flowersNumber * narcissusPrice);
				
				if(flowersNumber < 120) {
					totalSum = totalSum + (totalSum * 0.15);
				}
			break;
			
			case "Gladiolus":
				totalSum = (flowersNumber * gladiolusPrice);
				
				if(flowersNumber < 80) {
					totalSum = totalSum + (totalSum * 0.20);
				}
			break;
		}
		
		remainder = budget - totalSum;
		
		if(remainder >= 0) {
			message = "Hey, you have a great garden with "+ flowersNumber + " " + flowersType + " and " + String.format("%.2f", remainder) + " leva left.";
		} else {
			message = "Not enough money, you need " + String.format("%.2f", Math.abs(remainder)) + " leva more.";
		}
		
		System.out.println(message);
		
		input.close();
	}

}
