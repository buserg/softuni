package simpleOperationsAndCalculationsExercise;

import java.util.Scanner;

public class TwoDRectangleArea {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double x1 = input.nextDouble();
		double y1 = input.nextDouble();
		double x2 = input.nextDouble();
		double y2 = input.nextDouble();
		
		double a = Math.abs(x1 - x2);
		double b = Math.abs(y1 - y2);
		
		double area = a * b;
		double perimeter = 2 * (a + b);
		
		System.out.printf("%.2f", area);
		System.out.println();
		System.out.printf("%.2f", perimeter);
		
		input.close();
	}

}
