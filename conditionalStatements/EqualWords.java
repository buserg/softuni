package conditionalStatements;

import java.util.Scanner;

public class EqualWords {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String word1 = input.nextLine();
		String word2 = input.nextLine();
		
		if(word1.equalsIgnoreCase(word2)) {
			System.out.println("yes");
		} else {
			System.out.println("no");
		}
		
		input.close();
	}

}
