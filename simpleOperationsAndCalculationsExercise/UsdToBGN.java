package simpleOperationsAndCalculationsExercise;

import java.util.Scanner;

public class UsdToBGN {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double usd = input.nextDouble();
		double excangeRate = 1.79549;
		double bgn = usd * excangeRate;
		
		System.out.printf("%.2f", bgn);
		
		input.close();
	}

}
