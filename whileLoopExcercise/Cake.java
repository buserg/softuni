package whileLoopExcercise;

import java.util.Scanner;

public class Cake {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int n1 = Integer.parseInt(input.nextLine());
		int n2 = Integer.parseInt(input.nextLine());
		int sum = n1 * n2;

		while(sum >= 0)
		{
			String n3 = input.nextLine();
			if(n3.equals("STOP")) {
				break;
			}
			sum -= Integer.parseInt(n3);		
			
		}
		if(sum >= 0) {
			System.out.printf("%d pieces are left.", sum);
		} else {
			System.out.printf("No more cake left! You need %d pieces more.", (sum * -1));
		}
	
		input.close();
	}
}
