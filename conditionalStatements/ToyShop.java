package conditionalStatements;

import java.util.Scanner;

public class ToyShop {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double excursionPrice = input.nextDouble();
		int puzzleNumber = input.nextInt();
		int dollNumber = input.nextInt();
		int bearsNumber = input.nextInt();
		int minionsNumber = input.nextInt();
		int trucksNumber = input.nextInt();
		
		double sum = (puzzleNumber * 2.60) + (dollNumber * 3) + 
					 (bearsNumber * 4.10) + (minionsNumber * 8.20) + (trucksNumber * 2);
		
		int toys = puzzleNumber + dollNumber + bearsNumber + minionsNumber + trucksNumber;
		double discount = 0;
		
		if(toys >= 50) {
			discount = (25 * sum) / 100;
		}
		
		double totalSum = sum - discount;
		double rent = (totalSum * 10 ) / 100;
		double profit = totalSum - rent;
		
		if(profit >= excursionPrice) {
			System.out.printf("Yes! %.2f lv left.", (profit - excursionPrice));
		} else {
			System.out.printf("Not enough money! %.2f lv needed.", Math.abs(profit - excursionPrice));
		}
		
		input.close();
	}

}
