package conditionalStatements;

import java.util.Scanner;

public class GreaterNumber {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int firstNumber = input.nextInt();
		int secondNumber = input.nextInt();
		
		if(firstNumber > secondNumber) {
			System.out.println(firstNumber);
		} else {
			System.out.println(secondNumber);
		}
		
		input.close();
	}

}
