package nestedConditionalStatementsExercise;

import java.util.Scanner;

public class FishingBoat {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int groupBudget = Integer.parseInt(input.nextLine());
		String season = input.nextLine();
		int fisherMen = Integer.parseInt(input.nextLine());
		double totalSum = 0;
		double remainder = 0;
		
		double springPrice = 3000;
		double summerPrice = 4200;
		double autumnPrice = 4200;
		double winterPrice = 2600;
		
		if(season.equals("Spring")) {
			totalSum = springPrice;
			if(fisherMen <= 6) {
				totalSum -= (springPrice * 0.10);
			} else if(fisherMen > 6 && fisherMen <= 11) {
				totalSum -= (springPrice * 0.15);
			} else {
				totalSum -= (springPrice * 0.25);
			}
		} else if(season.equals("Summer")) {
			totalSum = summerPrice;
			if(fisherMen <= 6) {
				totalSum -= (summerPrice * 0.10);
			} else if(fisherMen > 6 && fisherMen <= 11) {
				totalSum -= (summerPrice * 0.15);
			} else {
				totalSum -= (summerPrice * 0.25);
			}
		} else if(season.equals("Autumn")) {
			totalSum = autumnPrice;
			if(fisherMen <= 6) {
				totalSum -= (autumnPrice * 0.10);
			} else if(fisherMen > 6 && fisherMen <= 11) {
				totalSum -= (autumnPrice * 0.15);
			} else {
				totalSum -= (autumnPrice * 0.25);
			}
		} else if(season.equals("Winter")) {
			totalSum = winterPrice;
			if(fisherMen <= 6) {
				totalSum -= (winterPrice * 0.10);
			} else if(fisherMen > 6 && fisherMen <= 11) {
				totalSum -= (winterPrice * 0.15);
			} else {
				totalSum -= (winterPrice * 0.25);
			}
		}
		
		if(fisherMen % 2 == 0 && !season.equals("Autumn")) {
			totalSum *= 0.95;
		}
		
		remainder = groupBudget - totalSum;
		
		if(remainder >= 0) {
			System.out.printf("Yes! You have %.2f leva left.", remainder);
		} else {
			System.out.printf("Not enough money! You need %.2f leva.", Math.abs(remainder));
		}
		
		input.close();
	}

}
