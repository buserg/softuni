package simpleOperationsAndCalculations;

import java.util.Scanner;

public class InchToCentimeters {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double coeficient = 2.54;
		double inch = input.nextDouble();
		
		double centimeters = coeficient * inch;
		
		System.out.printf("%.2f", centimeters);
		
		input.close();
	}

}
