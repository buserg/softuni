package forLoopExercise;

import java.util.Scanner;

public class DividewWithoutRemainder {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int n = Integer.parseInt(input.nextLine());
		double countP1 = 0;
		double countP2 = 0;
		double countP3 = 0;
		int number = 0;
		
		for (int i = 0; i < n; i++) {
			number = Integer.parseInt(input.nextLine());
			
			if(number % 2 == 0) {
				countP1++;
			}
			
			if(number % 3 == 0) {
				countP2++;
			}
			
			if(number % 4 == 0) {
				countP3++;
			}
		}
		
		System.out.printf("%.2f%%\n", ((countP1 / n) * 100));
		System.out.printf("%.2f%%\n", ((countP2 / n) * 100));
		System.out.printf("%.2f%%\n", ((countP3 / n) * 100));
		
		input.close();
	}
}