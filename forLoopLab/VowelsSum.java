package forLoopLab;

import java.util.Scanner;

public class VowelsSum {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String vowels = "aeiou";
		
		String word = input.nextLine();
		int sum = 0;
		
		for (int i = 0; i < vowels.length(); i++) {
			
			for (int j = 0; j < word.length(); j++) {
				if(vowels.charAt(i) == word.charAt(j)) {
					sum += (i + 1);
				}
			}
		}
		
		System.out.println(sum);
		
		input.close();
	}

}