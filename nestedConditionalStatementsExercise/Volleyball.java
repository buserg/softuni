package nestedConditionalStatementsExercise;

import java.util.Scanner;

public class Volleyball {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String typeYear = input.nextLine();
		int p = input.nextInt();
		int h = input.nextInt();
		double totalWeekends = 48;
		double sofiaWeekends = totalWeekends - h;
		
		double totalGames = 0;
		double sofiaGames = sofiaWeekends * (3.0/4.0);
		double holidaysGames = p * (2.0 / 3.0);
		
		totalGames = sofiaGames + holidaysGames + h;
		
		
		switch(typeYear) {
			case "leap":
				totalGames += totalGames * 0.15;
			
			default:
				System.out.printf("%.0f", Math.floor(totalGames));
			break;
		}
		
		input.close();
	}

}
