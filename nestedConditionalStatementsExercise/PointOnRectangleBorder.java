package nestedConditionalStatementsExercise;

import java.util.Scanner;

public class PointOnRectangleBorder {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double x1 = input.nextDouble();
		double y1 = input.nextDouble();
		double x2 = input.nextDouble();
		double y2 = input.nextDouble();
		double x = input.nextDouble();
		double y = input.nextDouble();
		String message = "";
		
		
		if((x == x1 || x == x2) && (y >= y1 && y <= y2)) {
			message = "Border";
		} else if((y == y1 || y == y2) && (x >= x1 && x <= x2)) {
			message = "Border";
		} else {
			message = "Inside / Outside";
		}
		
		System.out.println(message);
		
		input.close();
	}

}
