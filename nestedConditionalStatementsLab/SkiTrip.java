package nestedConditionalStatementsLab;

import java.util.Scanner;

public class SkiTrip {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int days = Integer.parseInt(scanner.nextLine());
		
		String room = scanner.nextLine();
		String valuation = scanner.nextLine();
		
		double totalPrice = 0;
		
		if(room.equals("room for one person")) {
			double overnight = 18.00;
			days -= 1;
			
			if(days <= 10) {
				totalPrice = days * overnight;
			} else if(days > 10 && days <= 15) {
				totalPrice = days * overnight;
			} else if(days > 15) {
				totalPrice = days * overnight;
			}
			
			if(valuation.equals("positive")) {
				totalPrice = totalPrice + ((totalPrice * 25) / 100);
			} else if(valuation.equals("negative")) {
				totalPrice = totalPrice - ((totalPrice * 10) / 100);
			}
		} else if(room.equals("apartment")) {
			double overnight = 25.00;
			days -= 1;
			if(days <= 10) {
				totalPrice = days * overnight;
				totalPrice = totalPrice - ((totalPrice * 30) / 100);
			} else if(days > 10 && days <= 15) {
				totalPrice = days * overnight;
				totalPrice = totalPrice - ((totalPrice * 35) / 100);
			} else if(days > 15) {
				totalPrice = days * overnight;
				totalPrice = totalPrice - ((totalPrice * 50) / 100);
			}
			
			if(valuation.equals("positive")) {
				totalPrice = totalPrice + ((totalPrice * 25) / 100);
			} else if(valuation.equals("negative")) {
				totalPrice = totalPrice - ((totalPrice * 10) / 100);
			}
		} else if(room.equals("president apartment")) {
			double overnight = 35.00;
			days -= 1;
			
			if(days <= 10) {
				totalPrice = days * overnight;
				totalPrice = totalPrice - ((totalPrice * 10) / 100);
			} else if(days > 10 && days <= 15) {
				totalPrice = days * overnight;
				totalPrice = totalPrice - ((totalPrice * 15) / 100);
			} else if(days > 15) {
				totalPrice = days * overnight;
				totalPrice = totalPrice - ((totalPrice * 20) / 100);
			}
			
			if(valuation.equals("positive")) {
				totalPrice = totalPrice + ((totalPrice * 25) / 100);
			} else if(valuation.equals("negative")) {
				totalPrice = totalPrice - ((totalPrice * 10) / 100);
			}
		}
		
		System.out.printf("%.2f", totalPrice);
		
		scanner.close();
	}
}