package whileLoopLab;

import java.util.Scanner;

public class NumberInRange {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int number = input.nextInt();
		
		while(number < 1 || number > 100) {
			System.out.println("Invalid number");
			number = input.nextInt();
		}
		
		System.out.println("The number is: " + number);
		
		input.close();
	}

}
