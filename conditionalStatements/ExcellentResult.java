package conditionalStatements;

import java.util.Scanner;

public class ExcellentResult {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double valuation = input.nextDouble();
		
		if(valuation >= 5.50){
			System.out.println("Excellent!");
		}
		
		input.close();
	}

}
