package nestedConditionalStatementsExercise;

import java.util.Scanner;

public class Journey {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double budget = Double.parseDouble(input.nextLine());
		String season = input.nextLine();
		double remainder = 0;
		String destination = "";
		String dwelling = "";
		
		if(budget <= 100) {
			if(season.equals("summer")) {
				remainder = budget * 0.30;
				destination = "Bulgaria";
				dwelling = "Camp";
			} else if(season.equals("winter")) {
				remainder = budget * 0.70;
				destination = "Bulgaria";
				dwelling = "Hotel";
			}
		} else if(budget > 100 && budget <= 1000) {
			if(season.equals("summer")) {
				remainder = budget * 0.40;
				destination = "Balkans";
				dwelling = "Camp";
			} else if(season.equals("winter")) {
				remainder = budget * 0.80;
				destination = "Balkans";
				dwelling = "Hotel";
			}
		} else if(budget > 1000) {
			remainder = budget * 0.90;
			destination = "Europe";
			dwelling = "Hotel";
		}
		
		System.out.printf("Somewhere in %s\n%s - %.2f", destination, dwelling, remainder);
		
		input.close();
	}

}
