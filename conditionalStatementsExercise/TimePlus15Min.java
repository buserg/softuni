package conditionalStatementsExercise;

import java.util.Scanner;

public class TimePlus15Min {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int hour = input.nextInt();
		int min = input.nextInt();
		
		int minAfter15 = (min + 15) % 60;
		
		if((min + 15) > 59) {
			hour += 1;
		}
		
		if(hour >= 24) {
			hour -= 24;
		}
		
		System.out.printf("%d:%02d", hour, minAfter15);
		
		input.close();
	}

}
