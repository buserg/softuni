package conditionalStatementsExercise;

import java.util.Scanner;

public class BonusScore {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double number = input.nextDouble();
		double bonus = 0;
		
		if(number <= 100) {
			bonus += 5;
		} else if(number > 100 && number <= 1000) {
			bonus = ((number * 20) / 100);
		} else if(number > 1000) {
			bonus += ((number * 10) / 100);
		}
		
		if(number % 2 == 0) {
			bonus += 1;
		}
		
		if((number % 10) == 5) {
			bonus += 2;
		}
		
		double score = number + bonus;
		
		
		System.out.println(bonus);
		System.out.println(score);
		
		input.close();
	}

}
