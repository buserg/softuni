package forLoopExercise;

import java.util.Scanner;

public class Salary {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int numberTabs = Integer.parseInt(input.nextLine());
		int salary = Integer.parseInt(input.nextLine());
		int facebookPenalty = 150;
		int instagramPenalty = 100;
		int redditPenalty = 50;
		
		String tab = "";
		
		for (int i = 0; i < numberTabs; i++) {
			tab = input.nextLine();
			
			if(tab.equals("Facebook")) {
				salary -= facebookPenalty;
			} else if(tab.equals("Instagram")) {
				salary -= instagramPenalty;
			} else if(tab.equals("Reddit")) {
				salary -= redditPenalty;
			}
		}
		
		if(salary <= 0) {
			System.out.println("You have lost your salary.");
		} else {
			System.out.println(salary);
		}
		
		input.close();
	}
}