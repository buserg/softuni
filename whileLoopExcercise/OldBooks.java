package whileLoopExcercise;

import java.util.Scanner;

public class OldBooks{
  public static void main(String[] args){
    Scanner input = new Scanner(System.in);

    String searchBook = input.nextLine();
    int allBooks = Integer.parseInt(input.nextLine());
    String book = "";
    int counter = 0;
    boolean isFound = false;

    while(allBooks > 0){
      book = input.nextLine();
      
      if(searchBook.equals(book)){
        isFound = true;
        break;
      }
      
      counter++;
      allBooks--;
    }

    if(isFound){
      System.out.println("You checked " + counter + " books and found it.");
    } else {
      System.out.println("The book you search is not here!\nYou checked " + counter + " books.");
    }

    input.close();
  }
}
