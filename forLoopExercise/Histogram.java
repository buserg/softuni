package forLoopExercise;

import java.util.Scanner;

public class Histogram {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int n = Integer.parseInt(input.nextLine());
		double countP1 = 0;
		double countP2 = 0;
		double countP3 = 0;
		double countP4 = 0;
		double countP5 = 0;
		int number = 0;
		
		for (int i = 0; i < n; i++) {
			number = Integer.parseInt(input.nextLine());
			
			if(number > 0 && number < 200) {
				countP1++;
			} else if(number >= 200 && number < 400) {
				countP2++;
			} else if(number >= 200 && number < 600) {
				countP3++;
			} else if(number >= 600 && number < 800) {
				countP4++;
			} else if(number >= 800) {
				countP5++;
			}
		}
		
		double p1 = (countP1 / n) * 100.0;
		double p2 = (countP2 / n) * 100.0;
		double p3 = (countP3 / n) * 100.0;
		double p4 = (countP4 / n) * 100.0;
		double p5 = (countP5 / n) * 100.0;
		
		System.out.printf("%.2f%%\n", p1);
		System.out.printf("%.2f%%\n", p2);
		System.out.printf("%.2f%%\n", p3);
		System.out.printf("%.2f%%\n", p4);
		System.out.printf("%.2f%%\n", p5);
		
		input.close();
	}
}