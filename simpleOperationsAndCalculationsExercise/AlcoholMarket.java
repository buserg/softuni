package simpleOperationsAndCalculationsExercise;

import java.util.Scanner;

public class AlcoholMarket {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double wiskeyPrice = input.nextDouble();
		double rakiaPrice = wiskeyPrice / 2;
		double beer  = input.nextDouble();
		double wine = input.nextDouble();
		double rakia = input.nextDouble();
		double wiskey = input.nextDouble();
		
		double priceWine = rakiaPrice - (0.4 * rakiaPrice);
		double priceBeer = rakiaPrice - (0.8 * rakiaPrice);
		
		double sumRakia = rakia * rakiaPrice;
		double sumWine = wine * priceWine;
		double sumBeer = beer * priceBeer;
		double sumWiskey = wiskey * wiskeyPrice;
		
		double totalSum = sumRakia + sumBeer + sumWine +sumWiskey;
		
		System.out.printf("%.2f", totalSum);
		
		input.close();
	}

}
