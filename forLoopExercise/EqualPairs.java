package forLoopExercise;

import java.util.Scanner;

public class EqualPairs {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int n = Integer.parseInt(input.nextLine());
		int firstNumber = 0;
		int secondNumber = 0;
		int sum = 0;
		int count = 1;
		String result = "";
		
		for (int i = 0; i < n; i++) {
			sum = firstNumber + secondNumber;
			firstNumber = Integer.parseInt(input.nextLine());
			secondNumber = Integer.parseInt(input.nextLine());
			
			if(sum == (firstNumber + secondNumber)) {
				count++;
			}
		}
		
		if(count == n) {
			result = "Yes, value=" + (firstNumber + secondNumber);
		} else {
			result = "No, maxdiff=" + Math.abs(sum - (firstNumber + secondNumber));
		}
		
		System.out.println(result);
		
		
		input.close();
	}
}