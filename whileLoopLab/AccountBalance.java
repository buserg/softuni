package whileLoopLab;

import java.util.Scanner;

public class AccountBalance {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double total = 0;
		int numberOperation = Integer.parseInt(input.nextLine());
		
		while(numberOperation > 0) {
			double sum = Double.parseDouble(input.nextLine());
			
			if(sum < 0) {
				System.out.println("Invalid operation!");
				break;
			}
			
			System.out.printf("Increase: %.2f\n", sum);
			
			total += sum;
			
			numberOperation--;
		}
		System.out.printf("Total: %.2f", total);
		
		input.close();
	}

}
