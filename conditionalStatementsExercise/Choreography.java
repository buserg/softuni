package conditionalStatementsExercise;

import java.util.Scanner;

public class Choreography {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double steps = input.nextDouble();
		double dancers = input.nextDouble();
		double days = input.nextDouble();
		
		double totalPercentStepsDay = Math.ceil((((steps / days) / steps) * 100));
		double percentStepsDancer = totalPercentStepsDay / dancers;
		
		if(totalPercentStepsDay <  13) {
			System.out.printf("Yes, they will succeed in that goal! %.2f%%.", percentStepsDancer);
		} else {
			System.out.printf("No, they will not succeed in that goal! Required %.2f%% steps to be learned per day.", percentStepsDancer);
		}
		
		input.close();
	}

}
