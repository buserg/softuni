package whileLoopLab;

import java.util.Scanner;

public class NumberSequence {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		String number = "";
		int stringToInt = 0;
		
		while(true) {
			number = input.nextLine();
			
			if(number.equals("END")) {
				break;
			} 
			
			stringToInt = Integer.parseInt(number);
			if(min >= stringToInt) {
				min = stringToInt;
			}
			
			if(max <= stringToInt) {
				max = stringToInt;
			}
		}
		
		System.out.println("Max number: " + max);
		System.out.println("Min number: " + min);
		
		input.close();
	}

}
