package simpleOperationsAndCalculationsExercise;

import java.util.Scanner;

public class TailoringWorkshop {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int table = input.nextInt();
		double tableLength = input.nextDouble();
		double tableHeight = input.nextDouble();
		
		double m2 = table * (tableLength + (2 * 0.30)) * (tableHeight + (2 * 0.30));
		double area = table * (tableLength / 2) * (tableLength / 2);
		
		double usd = (m2 * 7) + (area * 9);
		double bgn = usd * 1.85;
		
		System.out.printf("%.2f USD", usd);
		System.out.println();
		System.out.printf("%.2f BGN", bgn);
		
		input.close();
	}

}
