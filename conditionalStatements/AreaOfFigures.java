package conditionalStatements;

import java.util.Scanner;

public class AreaOfFigures {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String figure = input.nextLine();
		
		if(figure.equals("square")) {
			double side = input.nextDouble();
			double area = (side * side);
			
			System.out.printf("%.3f", area);
		} else if(figure.equals("rectangle")) {
			double sideA = input.nextDouble();
			double sideB = input.nextDouble();
			double area = (sideA * sideB);
			
			System.out.printf("%.3f", area);
		} else if(figure.equals("circle")) {
			double r = input.nextDouble();
			double area = Math.PI * (r * r);
			
			System.out.printf("%.3f", area);
		} else if(figure.equals("triangle")) {
			double sideA = input.nextDouble();
			double sideB = input.nextDouble();
			double area = (sideA * sideB) / 2;
			
			System.out.printf("%.3f", area);
		}
		
		input.close();
	}

}
