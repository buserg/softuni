package conditionalStatementsExercise;

import java.util.Scanner;

public class Scholarship {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double income = input.nextDouble();
		double mark = input.nextDouble();
		double minSalary = input.nextDouble();
		double socialScholarship = ((minSalary * 35) / 100);
		
		if(income <= minSalary && mark >= 4.50) {
			System.out.println("You get a Social scholarship " + (int)socialScholarship + " BGN");
		} else if(mark >= 5.50 && income <= minSalary ) {
			double scholarship = ((mark * 25));
			
			System.out.printf("You get a scholarship for excellent results %.2f BGN", scholarship);
		} else if(income > minSalary || mark <= 4.50){
			System.out.println("You cannot get a scholarship!");
		}
		
		input.close();
	}

}
