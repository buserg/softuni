package conditionalStatementsExercise;

import java.util.Scanner;

public class WorldSwimmingRecord {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double recordsInSeconds = input.nextDouble();
		double distance = input.nextDouble();
		double timeInSecondsOneMeter = input.nextDouble();
		
		double ivansTime = distance * timeInSecondsOneMeter;
		double delay = (Math.floor(distance / 15) * 12.5);
		double totalTime = ivansTime + delay;
		
		if(recordsInSeconds < totalTime) {
			System.out.printf("No, he failed! He was %.2f seconds slower.", (totalTime - recordsInSeconds));
		}
		
		if(recordsInSeconds > totalTime){
			System.out.printf("Yes, he succeeded! The new world record is %.2f seconds.", totalTime);
		}
		
		input.close();
	}

}
