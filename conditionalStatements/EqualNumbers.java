package conditionalStatements;

import java.util.Scanner;

public class EqualNumbers {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int a = input.nextInt();
		int b = input.nextInt();
		int c = input.nextInt();
		
		if(a == b && b == c) {
			System.out.println("yes");
		} else {
			System.out.println("no");
		}
		
		input.close();
	}

}
