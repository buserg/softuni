package nestedConditionalStatementsLab;

import java.util.Scanner;

public class FruitShop {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String fruit = input.nextLine();
		String day = input.nextLine();
		double quantity = input.nextDouble();
		double price = 0;
		
		if(day.equals("Monday") || day.equals("Tuesday") || day.equals("Wednesday") || day.equals("Thursday") || day.equals("Friday")) {
			if(fruit.equals("banana")) {
				price = quantity * 2.50;
			} else if(fruit.equals("apple")){
				price = quantity * 1.20;
			} else if(fruit.equals("orange")){
				price = quantity * 0.85;
			} else if(fruit.equals("grapefruit")){
				price = quantity * 1.45;
			} else if(fruit.equals("kiwi")){
				price = quantity * 2.70;
			} else if(fruit.equals("pineapple")){
				price = quantity * 5.50;
			} else if(fruit.equals("grapes")){
				price = quantity * 3.85;
			}
		} else if(day.equals("Saturday") || day.equals("Sunday")) {
			if(fruit.equals("banana")) {
				price = quantity * 2.70;
			} else if(fruit.equals("apple")){
				price = quantity * 1.25;
			} else if(fruit.equals("orange")){
				price = quantity * 0.90;
			} else if(fruit.equals("grapefruit")){
				price = quantity * 1.60;
			} else if(fruit.equals("kiwi")){
				price = quantity * 3.00;
			} else if(fruit.equals("pineapple")){
				price = quantity * 5.60;
			} else if(fruit.equals("grapes")){
				price = quantity * 4.20;
			}
		} 
		
		if(price == 0.00){
			System.out.println("error");
		} else {
			System.out.printf("%.2f", price);
		}
		
		
		
		input.close();
	}

}
