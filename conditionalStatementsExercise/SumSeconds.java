package conditionalStatementsExercise;

import java.util.Scanner;

public class SumSeconds {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int firstTime = input.nextInt();
		int secondTime = input.nextInt();
		int thirdTime = input.nextInt();
		
		int totalTime = firstTime + secondTime + thirdTime;
		int min = totalTime / 60;
		int seconds = totalTime % 60;
		
		System.out.printf("%d:%02d", min, seconds);
		
		input.close();
	}
}
