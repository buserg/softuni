package whileLoopExcercise;

import java.util.Scanner;

public class ExamPreparation {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int poorGrades = Integer.parseInt(input.nextLine());
		String exName = "";
		Double mark = 0.00;
		int countPoorGrades = 0;
		double totalSum = 0;
		String lastExName = "";
		int numberOfProblems = 0;
		
		while(poorGrades > countPoorGrades) {
			exName = input.nextLine();
			
			if(exName.equals("Enough")) {
				break;
			}
			
			
			mark = Double.parseDouble(input.nextLine());
			
			if(mark <= 4) {
				countPoorGrades++;
			}
			
			numberOfProblems++;
			lastExName = exName;
			totalSum += mark;
		}
		
		if(countPoorGrades < poorGrades) {
			System.out.printf("Average score: %.2f\nNumber of problems: %d\nLast problem: %s", (totalSum / numberOfProblems), numberOfProblems, lastExName);
		} else {
			System.out.printf("You need a break, %d poor grades.", countPoorGrades);
		}
		
		
		input.close();
	}

}