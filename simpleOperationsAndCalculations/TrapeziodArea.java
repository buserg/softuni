package simpleOperationsAndCalculations;

import java.util.Scanner;

public class TrapeziodArea {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double b1 = input.nextDouble();
		double b2 = input.nextDouble();
		double h = input.nextDouble();
		double area = (b1 + b2) * h / 2;
		
		System.out.printf("%.2f", area);
		
		input.close();
	}

}
