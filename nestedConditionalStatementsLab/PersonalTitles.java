package nestedConditionalStatementsLab;

import java.util.Scanner;

public class PersonalTitles {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double age = input.nextDouble();
		String sex = input.next();
		String title = "";
		
		if(age >= 16) {
			if(sex.equals("m")) {
				title = "Mr.";
			} else if(sex.equals("f")) {
				title = "Ms.";
			}
		} else {
			if(sex.equals("m")) {
				title = "Master";
			} else if(sex.equals("f")) {
				title = "Miss";
			}
		}
		
		System.out.println(title);
		
		input.close();
	}

}
