package whileLoopLab;

import java.util.Scanner;

public class Graduation {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String name = input.nextLine();
		double mark = 0;
		double totalSum = 0;
		int counter = 1;
		
		while(counter <= 12) {
			mark = Double.parseDouble(input.nextLine());
			
			if(mark < 4) {
				continue;
			} else {
				totalSum += mark;
			}
			
			counter++;
		}
		
		System.out.printf("%s graduated. Average grade: %.2f", name, (totalSum / 12));
		
		input.close();
	}

}
