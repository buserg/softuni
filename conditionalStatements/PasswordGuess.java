package conditionalStatements;

import java.util.Scanner;

public class PasswordGuess {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String correctPassword = "s3cr3t!P@ssw0rd";
		String password = input.nextLine();
		
		if(password.equals(correctPassword)) {
			System.out.println("Welcome");
		} else {
			System.out.println("Wrong password!");
		}
		
		input.close();
	}

}
