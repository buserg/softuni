package whileLoopLab;

import java.util.Scanner;

public class Walking {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int totalSteps = 0;
		String steps = "";
		
		while(totalSteps < 10000) {
			steps = input.nextLine();
			
			if(steps.equals("Going home")) {
				steps = input.nextLine();
				
				totalSteps += Integer.parseInt(steps);
				break;
			}
			
			totalSteps += Integer.parseInt(steps);
		}
		
		if(totalSteps < 10000) {
			System.out.printf("%d more steps to reach goal.", (10000 - totalSteps));
		} else {
			System.out.println("Goal reached! Good job!");
		}
		
		input.close();
	}

}
