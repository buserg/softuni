package conditionalStatementsExercise;

import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String flower = scanner.nextLine();
        int number = Integer.parseInt(scanner.nextLine());
        int budjet = Integer.parseInt(scanner.nextLine());
        double totalPrice = 0;
        switch (flower) {
            case "Roses":
                if (number < 80) {
                    totalPrice = 5 * number;
                } else if (number > 80) {
                    totalPrice = (number * 5) - (number * 5) * 0.10;
                }
                break;
            case "Dahlias":
                if (number < 90) {
                    totalPrice = 3.80 * number;
                } else if (number > 90) {
                    totalPrice = (number * 3.80) - (number * 3.80) * 0.15;
                }
                break;
            case "Tulips":
                if (number < 80) {
                    totalPrice = 2.80 * number;
                } else if (number > 80) {
                    totalPrice = (number * 2.80) - (number * 2.80) * 0.15;
                }
                break;
            case "Narcissus":
                if (number > 120) {
                    totalPrice = 3 * number;
                } else if (number < 120) {
                    totalPrice = (number * 3) + (number * 3) * 0.15;
                }
                break;
            case "Gladiolus":
                if (number > 80) {
                    totalPrice = 2.50 * number;
                } else if (number < 80) {
                    totalPrice = (number * 2.50) + (number * 2.50) * 0.20;
                }
                break;
        }
        if (totalPrice >= budjet) {
            double diference = totalPrice - budjet;
            System.out.printf("Not enough money, you need %.2f leva more.", diference);
        } else {
            double ostatuk = budjet - totalPrice;
            System.out.printf("Hey, you have a great garden with %d %s and %.2f leva left.", number, flower, ostatuk);
        }
    }
}
