package forLoopLab;

import java.util.Scanner;

public class MaxNumber {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int n = Integer.parseInt(input.nextLine());
		int max = Integer.MIN_VALUE ;
		int number = 0;
		
		for (int i = 0; i < n; i++) {
			number = Integer.parseInt(input.nextLine());
			if(max < number) {
				max = number;
			}
		}
		
		System.out.println(max);
		
		input.close();
	}

}