package simpleOperationsAndCalculations;

import java.util.Scanner;

public class SquareArea {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int a = input.nextInt();
		
		System.out.println(a * a);
		
		input.close();
	}

}
