package forLoopLab;

public class NumbersFrom1To100 {

	public static void main(String[] args) {
		int from = 1;
		int to = 100;
		
		for (int i = from; i <= to; i++) {
			System.out.println(i);
		}
	}
}
