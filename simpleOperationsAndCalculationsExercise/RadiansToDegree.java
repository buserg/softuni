package simpleOperationsAndCalculationsExercise;

import java.util.Scanner;

public abstract class RadiansToDegree {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double radians = input.nextDouble();
		double degree = radians * 180 / Math.PI;
		
		System.out.printf("%.0f", degree);
		
		input.close();
	}

}
