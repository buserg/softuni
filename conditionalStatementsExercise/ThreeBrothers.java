package conditionalStatementsExercise;

import java.util.Scanner;

public class ThreeBrothers {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double firstBrotherTime = input.nextDouble();
		double secondBrotherTime = input.nextDouble();
		double thirdBrotherTime = input.nextDouble();
		double fatherFishingTime = input.nextDouble();
		String message = "";
		
		double totalTime = 1 / ((1 / firstBrotherTime) + (1 / secondBrotherTime) + (1 / thirdBrotherTime));
		double totalTimeWithFreeTime = totalTime * 1.15;
		double remainingTime = fatherFishingTime - totalTimeWithFreeTime;
		
		if(remainingTime >= 0) {
			message = "Cleaning time: " + String.format("%.2f", totalTimeWithFreeTime) + "\nYes, there is a surprise - time left -> " + String.format("%.0f", Math.abs(Math.floor(remainingTime)))  + " hours.";
		} else {
			message = "Cleaning time: " + String.format("%.2f", totalTimeWithFreeTime) + "\nNo, there isn't a surprise - shortage of time -> " + String.format("%.0f", Math.abs(Math.floor(remainingTime)))  + " hours.";
		}
		
		System.out.println(message);
		
		input.close();
	}

}
