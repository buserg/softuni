package nestedConditionalStatementsExercise;

import java.util.Scanner;

public class HotelRoom {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String month = input.nextLine();
		int numberOfNights = input.nextInt();
		double totalSumApartment = 0;
		double totalSumStudio = 0;
		
		if(month.equals("May") || month.equals("October")) {
			totalSumApartment = numberOfNights * 65;
			totalSumStudio = numberOfNights * 50;
			
			if(numberOfNights > 14) {
				totalSumApartment -= totalSumApartment * 0.10;
			}
			
			if(numberOfNights > 7 && numberOfNights <= 14) {
				totalSumStudio -= totalSumStudio * 0.05;
			} else if(numberOfNights > 14) {
				totalSumStudio -= totalSumStudio * 0.30;
			}
		} else if(month.equals("June") || month.equals("September")) {
			totalSumApartment = numberOfNights * 68.70;
			totalSumStudio = numberOfNights * 75.20;
			
			if(numberOfNights > 14) {
				totalSumApartment -= totalSumApartment * 0.10;
				totalSumStudio -= totalSumStudio * 0.20;
			}
		} else if(month.equals("July") || month.equals("August")) {
			totalSumApartment = numberOfNights * 77;
			totalSumStudio = numberOfNights * 76;
			
			if(numberOfNights > 14) {
				totalSumApartment -= totalSumApartment * 0.10;
			}
		}
		
		System.out.printf(
			"Apartment: %.2f lv.\n" + 
			"Studio: %.2f lv." , totalSumApartment, totalSumStudio
		);
		
		input.close();
	}

}
