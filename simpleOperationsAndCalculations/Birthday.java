package simpleOperationsAndCalculations;

import java.util.Scanner;

public class Birthday {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double length = input.nextDouble();
		double width = input.nextDouble();
		double height = input.nextDouble();
		double sandPercent = input.nextDouble();
		double aquariumVolume = length * width * height;
		double decimeter = aquariumVolume * 0.001;
		double percent = sandPercent * 0.01;
		double aquariumAttributesValume = decimeter * (1 - percent);
		
		System.out.printf("%.3f", aquariumAttributesValume);
		
		input.close();
	}

}
