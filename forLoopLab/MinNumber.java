package forLoopLab;

import java.util.Scanner;

public class MinNumber {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int n = Integer.parseInt(input.nextLine());
		int min = Integer.MAX_VALUE;
		int number = 0;
		
		for (int i = 0; i < n; i++) {
			number = Integer.parseInt(input.nextLine());
			if(min > number) {
				min = number;
			}
		}
		
		System.out.println(min);
		
		input.close();
	}
}
