package nestedConditionalStatementsExercise;

import java.util.Scanner;

public class OperationsBetweenNumbers {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double firstNumber = input.nextDouble();
		double secondNumber = input.nextDouble();
		String operation = input.next();
		double sum = 0;
		String message = "";
		
		if(secondNumber == 0 && (operation.equals("/") || (operation.equals("%")))) {
			message = "Cannot divide " + String.format("%.0f", firstNumber) + " by zero";
		} else {	
			message = String.format("%.0f", firstNumber) + " " + operation + " " + String.format("%.0f", secondNumber) + " = ";
			switch(operation) {
				case "+": 
					sum = firstNumber + secondNumber;
					message += String.format("%.0f", sum);
					
					if(sum % 2  == 0) {
						message += " - even"; 
					} else {
						message += " - odd"; 
					}
				break;
				
				case "-": 
					sum = firstNumber - secondNumber;
					message += String.format("%.0f", sum);;
					
					if(sum % 2  == 0) {
						message += " - even"; 
					} else {
						message += " - odd"; 
					}
				break;
				
				case "*": 
					sum = firstNumber * secondNumber;
					message += String.format("%.0f", sum);
					
					if(sum % 2  == 0) {
						message += " - even"; 
					} else {
						message += " - odd"; 
					}
				break;
				
				case "/": 
					sum = firstNumber / secondNumber;
					message += String.format("%.2f", sum);
				break;
				
				case "%": 
					sum = firstNumber % secondNumber;
					message += String.format("%.0f", sum);
				break;
			}
		}
		
		System.out.println(message);
		
		input.close();
	}

}
