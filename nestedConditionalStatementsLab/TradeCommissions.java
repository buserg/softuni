package nestedConditionalStatementsLab;

import java.util.Scanner;

public class TradeCommissions {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String city = input.nextLine();
		double sales = input.nextDouble();
		double commission = 0;
		
		if(city.equals("Sofia")) {
			if(sales >= 0 && sales <= 500) {
				commission = (sales * 5) / 100;
			} else if(sales > 500 && sales <= 1000) {
				commission = (sales * 7) / 100;
			} else if(sales > 1000 && sales <= 10000) {
				commission = (sales * 8) / 100;
			} else if(sales > 10000) {
				commission = (sales * 12) / 100;
			}
		} else if(city.equals("Varna")) {
			if(sales >= 0 && sales <= 500) {
				commission = (sales * 4.5) / 100;
			} else if(sales > 500 && sales <= 1000) {
				commission = (sales * 7.5) / 100;
			} else if(sales > 1000 && sales <= 10000) {
				commission = (sales * 10) / 100;
			} else if(sales > 10000) {
				commission = (sales * 13) / 100;
			}
		} else if(city.equals("Plovdiv")) {
			if(sales >= 0 && sales <= 500) {
				commission = (sales * 5.5) / 100;
			} else if(sales > 500 && sales <= 1000) {
				commission = (sales * 8) / 100;
			} else if(sales > 1000 && sales <= 10000) {
				commission = (sales * 12) / 100;
			} else if(sales > 10000) {
				commission = (sales * 14.5) / 100;
			}
		}
		
		if(commission > 0) {
			System.out.printf("%.2f", commission);
		} else {
			System.out.println("error");
		}
		
		input.close();
	}

}
