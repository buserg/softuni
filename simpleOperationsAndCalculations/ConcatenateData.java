package simpleOperationsAndCalculations;

import java.util.Scanner;

public class ConcatenateData {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String firstName = input.nextLine();
		String lastName = input.nextLine();
		int age = Integer.parseInt(input.nextLine());
		String town = input.nextLine();
		
		System.out.println("You are " + firstName + " " + lastName + ", a " + age + "-years old person from " + town + ".");
		
		input.close();
	}

}
