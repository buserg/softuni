package conditionalStatements;

import java.util.Scanner;

public class AnimalType {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String animal = input.nextLine();
		String animalName = "";
		
		if(animal.equals("dog")) {
			animalName = "mammal";
		} else if(animal.equals("crocodile") || animal.equals("tortoise") || animal.equals("snake")) {
			animalName = "reptile";
		} else if(animal.equals("cat")){
			animalName = "unknown";
		}
		
		System.out.println(animalName);
		
		input.close();
	}

}
