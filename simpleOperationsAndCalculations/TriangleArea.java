package simpleOperationsAndCalculations;

import java.util.Scanner;

public class TriangleArea {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double a = input.nextDouble();
		double h = input.nextDouble();
		double area = a * h / 2;
		
		System.out.printf("%.2f", area);
		
		input.close();
	}

}
