package whileLoopLab;

import java.util.Scanner;

public class Moving {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int width = Integer.parseInt(input.nextLine());
		int length = Integer.parseInt(input.nextLine());
		int height = Integer.parseInt(input.nextLine());
		String numberCartons = "";
		int cubicMeters = width * length * height;
		int totalCartons = 0;
		int cartons = 0;
		
		while(totalCartons < cubicMeters) {
			numberCartons = input.nextLine();
			
			if(numberCartons.equals("Done")) {
				break;
			}

			cartons = Integer.parseInt(numberCartons);
			totalCartons += cartons;
		}
		
		if(totalCartons > cubicMeters) {
			System.out.printf("No more free space! You need %d Cubic meters more.", (totalCartons - cubicMeters));
		} else if(totalCartons <= cubicMeters){
			System.out.printf("%d Cubic meters left.", (cubicMeters - totalCartons));
		}
		
		
		input.close();
	}

}
