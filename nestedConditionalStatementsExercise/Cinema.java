package nestedConditionalStatementsExercise;

import java.util.Scanner;

public class Cinema {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String projection = input.nextLine();
		int rows = input.nextInt();
		int cols = input.nextInt();
		double premierePrice = 12.00;
		double normalPrice = 7.50;
		double discountPrice = 5.00;
		int totalPlaces = rows * cols;
		
		double income = 0;
		
		switch(projection) {
			case "Premiere": income = totalPlaces * premierePrice; break;
			case "Normal": income = totalPlaces * normalPrice; break;
			case "Discount": income = totalPlaces * discountPrice; break;
		}
		
		System.out.printf("%.2f leva", income);
		
		input.close();
	}
}
