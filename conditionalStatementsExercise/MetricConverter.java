package conditionalStatementsExercise;

import java.util.Scanner;

public class MetricConverter {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double number = input.nextDouble();
		String measurement1 = input.next();
		String measurement2 = input.next();
		double outputNumber = 0;
		
		
		if(measurement1.equals("mm") && measurement2.equals("cm")) {
			outputNumber += number / 10;
		} else if(measurement1.equals("mm") && measurement2.equals("m")) {
			outputNumber += number / 1000;
		} else if(measurement1.equals("cm") && measurement2.equals("m")) {
			outputNumber += number / 100;
		} else if(measurement1.equals("cm") && measurement2.equals("mm")) {
			outputNumber += number * 10;
		} else if(measurement1.equals("m") && measurement2.equals("mm")) {
			outputNumber += number * 1000;
		} else if(measurement1.equals("m") && measurement2.equals("cm")) {
			outputNumber += number * 100;
		}
		
		System.out.printf("%.3f", outputNumber);
		
		input.close();
	}

}
