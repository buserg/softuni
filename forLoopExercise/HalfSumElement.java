package forLoopExercise;

import java.util.Scanner;

public class HalfSumElement {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int n = Integer.parseInt(input.nextLine());
		int sum = 0;
		int max = Integer.MIN_VALUE;
		int number = 0;
		
		for (int i = 0; i < n; i++) {
			number = Integer.parseInt(input.nextLine());
			
			
			sum += number;
			
			if(max < number) {
				max = number;
			}
		}


		if(max == (sum - max)) {
			System.out.println("Yes\nSum = " + max);
		} else {
			System.out.println("No\nDiff = " + Math.abs((sum - max) - max));
		}
		
		input.close();
	}
}