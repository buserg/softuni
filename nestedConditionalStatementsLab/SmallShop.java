package nestedConditionalStatementsLab;

import java.util.Scanner;

public class SmallShop {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String article = input.nextLine();
		String city = input.nextLine();
		double quantity = input.nextDouble();
		double price = 0;
		
		if(article.equals("coffee")) {
			if(city.equals("Sofia")) {
				price = quantity * 0.50; 
			} else if(city.equals("Plovdiv")) {
				price = quantity * 0.40;
			} else if(city.equals("Varna")) {
				price = quantity * 0.45;
			}
		} else if(article.equals("water")) {
			if(city.equals("Sofia")) {
				price = quantity * 0.80; 
			} else if(city.equals("Plovdiv")) {
				price = quantity * 0.70;
			} else if(city.equals("Varna")) {
				price = quantity * 0.70;
			}
		} else if(article.equals("beer")) {
			if(city.equals("Sofia")) {
				price = quantity * 1.20; 
			} else if(city.equals("Plovdiv")) {
				price = quantity * 1.15;
			} else if(city.equals("Varna")) {
				price = quantity * 1.10;
			}
		} else if(article.equals("sweets")) {
			if(city.equals("Sofia")) {
				price = quantity * 1.45; 
			} else if(city.equals("Plovdiv")) {
				price = quantity * 1.30;
			} else if(city.equals("Varna")) {
				price = quantity * 1.35;
			}
		} else if(article.equals("peanuts")) {
			if(city.equals("Sofia")) {
				price = quantity * 1.60; 
			} else if(city.equals("Plovdiv")) {
				price = quantity * 1.50;
			} else if(city.equals("Varna")) {
				price = quantity * 1.55;
			}
		}
		
		System.out.println(price);
		
		input.close();
	}

}
