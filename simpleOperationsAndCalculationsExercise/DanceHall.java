package simpleOperationsAndCalculationsExercise;

import java.util.Scanner;

public class DanceHall {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double hallLength = input.nextDouble();
		double hallWidth = input.nextDouble();
		double side = input.nextDouble();
		
		double hallSize = (hallLength * 100) * (hallWidth * 100);
		double wardrobeSize = (side * 100) * (side * 100);
		double benchSize = hallSize / 10;
		double freeSpace =  hallSize - wardrobeSize - benchSize;
		
		System.out.printf("%.0f", Math.floor(freeSpace / (40 + 7000)));
		
		input.close();
	}

}
