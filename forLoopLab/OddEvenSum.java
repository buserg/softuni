package forLoopLab;

import java.util.Scanner;

public class OddEvenSum {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int n = Integer.parseInt(input.nextLine());
		int number = 0;
		int oddSum = 0;
		int evenSum = 0;
		
		for (int i = 1; i <= n; i++) {
			number = Integer.parseInt(input.nextLine());
			
			if(i % 2 == 0) {
				evenSum += number;
			} else {
				oddSum += number;
			}
		}
		
		if(evenSum == oddSum) {
			System.out.println("Yes\nSum = " + evenSum);
		} else {
			System.out.println("No\nDiff = " + Math.abs(evenSum - oddSum));
		}
		
		input.close();
	}
}